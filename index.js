window.onload = () => {
  "use strict";

  if ("serviceWorker" in navigator) {
    navigator.serviceWorker.register("./sw.js");
  }
};

function toggleDark() {
  var darkSwitch = document.getElementById("darkSwitch");
  document.getElementById("darkmode").disabled = !darkSwitch.checked;
}
toggleDark();

var specialnums = {
  "10": "diez",
  "11": "once",
  "12": "doce",
  "13": "trece",
  "14": "catorce",
  "15": "quince",
  "20": "veinte",
  "100": " cien",
  "1000": "mil"
};

var onedigitnums = {
  "0": "cero",
  "1": "uno",
  "2": "dos",
  "3": "tres",
  "4": "cuatro",
  "5": "cinco",
  "6": "seis",
  "7": "siete",
  "8": "ocho",
  "9": "nueve"
};

var ten_prefixes = {
  "0": "",
  "10": "dieci",
  "20": "veinti",
  "30": "treinta y ",
  "40": "cuarenta y ",
  "50": "cincuenta y ",
  "60": "sesenta y ",
  "70": "setenta y ",
  "80": "ochenta y ",
  "90": "noventa y "
};

var hundred_prefixes = {
  "0": "",
  "100": "dieci ",
  "200": "doscientos ",
  "300": "trescientos ",
  "400": "cuatrocientos ",
  "500": "quinientos ",
  "600": "seiscientos ",
  "700": "setecientos ",
  "800": "ochocientos ",
  "900": "novecientos "
};

function convert(inputvalue) {
  if (inputvalue in specialnums) {
    return specialnums[inputvalue];
  }
  if (inputvalue.length === 1) {
    return onedigitnums[inputvalue];
  }

  if (inputvalue < 100) {
    for (var key in ten_prefixes) {
      console.log(key, ten_prefixes[key]);
      if (Number(inputvalue) < key + 10 && Number(inputvalue) > key) {
        console.log("met");
        var result = ten_prefixes[key] + onedigitnums[inputvalue[1]];
      }
    }

    if (result) {
      return result;
    }
  }

  if (inputvalue < 1000) {
    for (var key_hundreds in hundred_prefixes) {
      if (inputvalue < Number(key_hundreds) + 100) {
        for (var key in ten_prefixes) {
          console.log(key, ten_prefixes[key]);
          if (
            Number(inputvalue.slice(1)) < key + 10 &&
            Number(inputvalue.slice(1)) > key
          ) {
            console.log("met");
            var result =
              ten_prefixes[key] + onedigitnums[inputvalue.slice(1)[1]];
          }
        }

        if (result) {
          return hundred_prefixes[key_hundreds] + result;
        }
      }
    }
  }
}

function calc() {
  var inputvalue = document.getElementById("number").value;
  var outputfield = document.getElementById("output");
  outputfield.innerHTML = convert(inputvalue);
}

$("#number").on("keyup", function(e) {
  calc();
});
